import enum


class InstrumentalExamination(enum.Enum):
    AbdominalUltrasound = 0
    CardiacPowerDopplerUltrasound = 1
    CardiacUltrasound = 2
    UltrasoundOfTheThyroidGland = 3
    MediastinalUltrasound = 4
    TransesophagealUltrasound = 5
    LiverUltrasound = 6
    KidneyUltrasound = 7
    UltrasoundOfThePancreas = 8
    UltrasoundOfTheSpleen = 9
    UltrasoundOfTheBladder = 10
    TesticularUltrasound = 11
    TransvaginalUltrasound = 12
    TransrectalUltrasound = 13
    DopplerUltrasoundOfTheVessels = 14
    UltrasoundOfTheJoint = 15
    TwelveLeadECG = 16
    CarotidUltrasound = 17
    BloodPressureMeasurement = 18
    Arteriography = 19
    CTAngiography = 20
    MRAngiography = 21
    RenalArteryDopplerUltrasound = 22
    GeneticTest = 23
    StressTest = 24
    EcoStress = 25
    CardiacCatheterization = 26
    CardiacBiopsy = 27

