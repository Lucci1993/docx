import enum


class Disease(enum.Enum):
    PrimaryArterialHypertension = 0
    RenovascularArterialHypertension = 1
    NephroparenchymalArterialHypertension = 2
    MiddleAorticSyndrome = 3
    IatrogenicArterialHypertension = 4
    LiddleSyndrome = 5
    GordonSyndrome = 6
    BilginturanSyndrome = 7
    GlucocorticoidRemediableAldosteronism = 8
    GitelmanSyndrome = 9
    StableAngina = 10
    UnstableAngina = 11
    TypicalAngina = 12
    AtypicalAngina = 13
    AcuteMyocardialInfarction = 14
    Tachyarrhythmia = 15
    MitralStenosis = 16
    MitralInsufficiency = 17
    AorticStenosis = 18
    AorticInsufficiency = 19
    TricuspidStenosis = 20
    TricuspidInsufficiency = 21
    PulmonaryStenosis = 22
    PulmonaryInsufficiency = 23
    HypertrophicCardiomyopathy = 24
    DilatedCardiomyopathy = 25
    RestrictiveCardiomyopathy = 26
    Myocarditis = 27
    Pericarditis = 28
    HeartFailure = 29
    AorticAneurysm = 30
    AorticDissection = 31
    AorticIntramuralHematoma = 32
    PenetratingAorticUlcer = 33








