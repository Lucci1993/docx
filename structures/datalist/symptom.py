import enum


class Symptom(enum.Enum):
    Sweating = 0
    Fever = 1
    Asthenia = 2
    WeightLoss = 3
    Dyspnea = 4
    DryCough = 5
    WetCough = 6
    Headache = 7
    NuchalStiffness = 8
    Epistaxis = 9
    Odynophagia = 10
    Vertigo = 11
    Syncope = 12
    ImpairedVision = 13
    TIA = 14 
    SensoryDeficit = 15
    MotorDeficit = 16
    Stroke = 17
    CognitiveImpairment = 18
    Dementia = 19
    ChestPain = 20
    ShortnessOfBreath = 21
    Edema = 22
    Palpitations = 23
    ArrhythmiasExpeciallyAF = 24
    HeartFailure = 25
    ArterialHypertensionS = 26
    RenalBruit = 27
    AsymmetricalKidneys = 28
    Nephrolithiasis = 29
    PulmonaryEdema = 30
    BilateralClaudicatio = 31
    ShortStature = 32
    Brachydactyly = 33
    MetacarpalAnomalies = 34
    SaltCraving = 35
    Dysuria = 36
    Nocturia = 37
    Tetany = 38
    AbdominalPain = 39
    ArterialHypotension = 40
    Chondrocalcinosis = 41
    Nausea = 42
    Vomiting = 43
    AttackLastsLessThan15Minutes = 44
    AttackLastsMoreThan20Minutes = 45
    ThePainSubsidesWithRest = 46
    WorseningOfSeverityWithEachEpisode = 47
    CoronaryArteryDisease = 48
    PulmonaryArterialHypertension = 49
    Haemoptysis = 50
    PulmonaryOedema = 51
    SystolicMurmurs = 52
    AnginaPectoris = 53
    DyastolicMurmurs = 54
    ParoxysmalNocturnalDyspnea = 55
    EdemaOfTheLowerLimbs = 56
    TurgorOfTheJugular = 57
    PositiveHepatojugularReflux = 58
    Hepatomegaly = 59
    Ascites = 60
    
    
    
