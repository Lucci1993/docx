import enum


class RiskFactor(enum.Enum):
    Pollution = 0
    Smoker = 1
    Obesity = 2
    Immobility = 3
    PoorPhysicalActivity = 4
    EstrogenProgestogenTherapy = 5
    ArterialHypertensionRF = 6
    DiabetesRF = 7
    Hypercholesterolemia = 8
    ChronicSunExposure = 9
    RecentTripsAbroad = 10
    FamilyHistoryOfHypertension = 11
    FamilyHistoryOfCardioVascularDisease = 12
    FamilyHistoryOfStroke = 13
    FamilyHistoryOfRenalDisease = 14
    FamilyHistoryOfDyslipidemia = 15
    HighSaltDiet = 16
    AlcoholConsumption = 17
    HistoryOfErectileDysfunction = 18
    HistoryOfSnoring = 19
    HistoryOfHypertensionInPregnancyOrPreeclampsia = 20
    Dyslipidemia = 21
    BPCORF = 22
    YoungAge = 23
    OldAge = 24
    HistoryOfRenalDisease = 25
    Polydipsia = 26
    ExcessiveIntakeOfLicorice = 27
    ExcessiveAdministrationOfIntravenousFluids = 28
    DrugsConsumption = 29
    MaleSex = 30
    FemaleSex = 31
    FamilyHistoryOfHypotension = 32
    CongenitalValvulopathies = 33
    InfectiveEndocarditisRF = 34
    ElectrolyteImbalances = 35
    HistoryOfHeartAttack = 36
    HistoryOfHeartFailure = 37
    Collagenopathies = 38
    RheumaticDisease = 39
    PulmonaryHypertension = 40
    CongenitalCardiomyopathy = 41
    Drug = 42
    Pregnancy = 43
    MyocarditisRF = 44
    AutoimmuneDiseases = 45
    Immunodeficiency = 46
    NutritionalDeficiencies = 47


    
    


