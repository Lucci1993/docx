import enum


class LaboratoryTests(enum.Enum):
    BloodVes = 0
    BloodPcr = 1
    BloodLdh = 3
    BloodAlt = 4
    BloodAst = 5
    BloodYgt = 6
    BloodAlkalinePhosphatase = 7
    BloodCreatinine = 8
    BloodPotassium = 9
    BloodSodium = 10
    BloodCalcium = 11
    BloodUreaNitrogen = 12
    BloodMagnesium = 13
    BloodTriglycerides = 14
    BloodTotalCholesterol = 15
    BloodLDLCholesterol = 16
    BloodHDLCholesterol = 17
    Hematocrit = 18
    RedBloodCellCount = 19
    Hemoglobin = 20
    MCV = 21
    MCH = 22
    MCHC = 23
    RDW = 24
    PlateletCount = 25
    WhiteBloodCellCount = 26
    BasophilsCount = 27
    EosinophilsCount = 28
    MonocytesCount = 29
    LymphocytesCount = 30
    NeutrophilsCount = 31
    BloodAldosterone = 32
    PlasmaReninActivity = 33
    BloodChloride = 34
    ACTHTest = 35
    BloodPh = 36
    CardiacTroponin = 37
    NTproBNP = 38
