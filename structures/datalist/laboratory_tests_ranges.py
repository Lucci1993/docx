import enum


# The first one is the min value and the second one is the max value beyond which is considered a pathological state
class LaboratoryTestsRanges(enum.Enum):
    BloodVes = [0, 28, "mm/h"]
    BloodPcr = [0, 8, "mg/l"]
    BloodLdh = [80, 300, "mU/ml"]
    BloodAlt = [7, 55, "U/l"]
    BloodAst = [7, 48, "U/l"]
    BloodYgt = [9, 49, "U/l"]
    BloodAlkalinePhosphatase = [50, 220, "U/l"]
    BloodCreatinine = [0.8, 1.2, "mg/dl"]
    BloodPotassium = [3.5, 5.0, "mEq/l"]
    BloodSodium = [135, 145, "mmol/l"]
    BloodCalcium = [8.5, 10.5, "mg/dl"]
    BloodUreaNitrogen = [10.3, 21.4, "mg/dl"]
    BloodMagnesium = [1.41, 1.85, "mEq/l"]
    BloodTriglycerides = [0, 200, "mg/dl"]
    BloodTotalCholesterol = [0, 200, "mg/dl"]
    BloodLDLCholesterol = [0, 130, "mg/dl"]
    BloodHDLCholesterol = [40, 60, "mg/dl"]
    Hematocrit = {
        "M": [38.3, 48.6, "%"],
        "F": [35.4, 44.9, "%"],
    }
    RedBloodCellCount = {
        "M": [4.32, 5.72, "million cells/mcL"],
        "F": [3.90, 5.03, "million cells/mcL"],
    }
    Hemoglobin = {
        "M": [13.2, 16.6, "gr/dL"],
        "F": [11.6, 15, "gr/dL"],
    }
    MCV = [80, 100, "fL"]
    MCH = [26, 32, "pg"]
    MCHC = [32, 36, "g/dL"]
    RDW = [11.5, 14.5, "%"]
    PlateletCount = {
        "M": [135000, 317000, "platelet/mcL"],
        "F": [157000, 371000, "platelet/mcL"]
    }
    WhiteBloodCellCount = [3400, 9600, "cells/mcL"]
    BasophilsCount = [2, 150, "cells/mmc"]
    EosinophilsCount = [20, 600, "cells/mmc"]
    MonocytesCount = [100, 900, "cells/mmc"]
    LymphocytesCount = [1500, 5000, "cells/mmc"]
    NeutrophilsCount = [2000, 8000, "cells/mmc"]
    BloodAldosterone = {
        "Supine": [1.9, 25.7, "ng/dl"],
        "Orthostatism": [2.4, 40.3, "ng/dl"]
    }
    PlasmaReninActivity = {
        "Supine": [0.28, 3.99, "ng/dl"],
        "Orthostatism": [0.44, 4.99, "ng/dl"]
    }
    BloodChloride = [98, 107, "mmol/l"]
    ACTHTest = [18, 999, "mcg/dl"]
    BloodPh = [7.36, 7.43, ""]
    CardiacTroponin = [0, 0.2, "mg/l"]
    NTproBNP = [0, 155, "ng/L"]
