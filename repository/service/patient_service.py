from repository import Repository
from repository.mongo import MongoRepository
import logging as log
from utils import data_validator as dt_val


class Service(object):
    def __init__(self, repo_client=Repository(adapter=MongoRepository)):
        self.repo_client = repo_client

    def find_patient(self, patient_id):
        patient = self.repo_client.find({'_id': patient_id})
        if patient:
            return patient
        else:
            return None

    def create_patient(self, patient):
        log.info("Validate patient: {}", patient)
        val = dt_val.validate_patient(patient)
        if val:
            return {"message": val, "data": False}
        created = self.repo_client.create(patient)
        if created.acknowledged:
            return {"message": None, "data": created.acknowledged}
        else:
            return {"message": "The patient can't be created", "data": created.acknowledged}

    def update_patient(self, patient):
        val = dt_val.validate_patient(patient)
        if val:
            return {"message": val, "data": False}

        el = self.find_patient(patient['_id'])
        if not el:
            return {"message": "Patient '{}' not present in DB".format(patient["_id"]), "data": False}

        if self.repo_client.update({'_id': patient['_id']}, patient) > 0:
            return {"message": None, "data": True}
        return {"message": "The patient can't be created", "data": False}

    def delete_patient(self, patient_id):
        records_affected = self.repo_client.delete({'_id': patient_id})
        return records_affected > 0
