from repository import Repository
from repository.mongo import MongoRepository
import logging as log
from utils import data_validator as dt_val


class Service(object):
    def __init__(self, repo_client=Repository(adapter=MongoRepository)):
        self.repo_client = repo_client

    @staticmethod
    def dump(data):
        data.pop("_id")
        return data

    def find_all_diseases(self, selector):
        diseases_list = self.repo_client.find_all(selector)
        return [self.dump(disease) for disease in diseases_list]

    def find_disease(self, disease_id):
        disease = self.repo_client.find({'id': disease_id})
        if disease:
            return self.dump(disease)
        else:
            return None

    def create_disease(self, disease):
        log.info("Validate disease: {}", disease)
        val = dt_val.validate_json(disease)
        if val:
            return {"message": val, "data": False}

        if self.find_disease(disease["id"]):
            return {"message": "Disease '{}' already present in DB".format(disease["id"]), "data": False}

        disease["_id"] = disease["id"]
        created = self.repo_client.create(disease)
        if created.acknowledged:
            return {"message": None, "data": created.acknowledged}
        else:
            return {"message": "The disease can't be created", "data": created.acknowledged}

    def update_disease(self, disease_id, disease):
        if not self.find_disease(disease["id"]):
            return False

        val = dt_val.validate_json(disease)
        if val:
            return {"message": val, "data": False}

        if self.repo_client.update({'id': disease_id}, disease) > 0:
            return {"message": None, "data": True}
        return {"message": "The disease can't be created", "data": False}

    def delete_disease(self, disease_id):
        records_affected = self.repo_client.delete({'id': disease_id})
        return records_affected > 0

    def delete_many(self, selector):
        self.repo_client.delete_all(selector)
