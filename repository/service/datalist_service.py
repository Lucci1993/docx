from repository import Repository
from repository.mongo import MongoRepository
import logging as log
from utils import datalist_validator as dt_val


class Service(object):
    def __init__(self, repo_client=Repository(adapter=MongoRepository)):
        self.repo_client = repo_client

    @staticmethod
    def dump(data):
        data.pop("_id")
        data.pop("type")
        return data

    def find_all_datalist(self, selector):
        datalist = self.repo_client.find_all(selector)
        return [self.dump(data) for data in datalist]

    def create_datalist(self, datalist):
        log.info("Validate datalist: {}", datalist)
        val = dt_val.validate_datalist(datalist)
        if val:
            return {"message": val, "data": False}

        el = self.find_all_datalist({'type': datalist['type'], 'name': datalist['name']})
        if el:
            return {"message": "Datalist '{}' already present in DB".format(el[0]["name"]), "data": False}

        else:
            data_list = self.find_all_datalist({'type': datalist['type']})
            if data_list:
                maximum = max(data_list, key=lambda x: x['id'])
                datalist['id'] = maximum['id'] + 1
            else:
                datalist['id'] = 0
            datalist["_id"] = datalist['type'] + "_" + str(datalist["id"])
            created = self.repo_client.create(datalist)
            if created.acknowledged:
                return {"message": None, "data": created.acknowledged}
            else:
                return {"message": "The datalist can't be created", "data": created.acknowledged}

    def update_datalist(self, datalist):
        val = dt_val.validate_datalist(datalist)
        if val:
            return {"message": val, "data": False}

        el = self.find_all_datalist({'type': datalist['type'], 'name': datalist['name']})
        if not el:
            return {"message": "Datalist '{}' not present in DB".format(datalist['name']), "data": False}

        if self.repo_client.update({'name': datalist['name']}, datalist) > 0:
            return {"message": None, "data": True}
        return {"message": "The datalist can't be created", "data": False}

    def delete_datalist(self, datalist_type, name):
        records_affected = self.repo_client.delete({'type': datalist_type, 'name': name})
        return records_affected > 0

    def delete_many(self, selector):
        self.repo_client.delete_all(selector)
