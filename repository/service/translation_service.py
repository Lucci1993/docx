from repository import Repository
from repository.mongo import MongoRepository
import logging as log
from utils import data_validator as dt_val


class Service(object):
    def __init__(self, repo_client=Repository(adapter=MongoRepository)):
        self.repo_client = repo_client

    @staticmethod
    def dump(data):
        data.pop("_id")
        data.pop("type")
        return data

    def find_all_translation(self, selector):
        translation = self.repo_client.find_all(selector)
        return [self.dump(data) for data in translation]

    def create_translation(self, translation):
        log.info("Validate datalist: {}", translation)
        val = dt_val.validate_translation(translation)
        if val:
            return {"message": val, "data": False}

        el = self.find_all_translation({'type': translation['type'], 'name': translation['name']})
        if el:
            return {"message": "Translation '{}' already present in DB".format(el[0]["id"]), "data": False}

        else:
            trans = self.find_all_translation({'type': translation['type']})
            if trans:
                maximum = max(trans, key=lambda x: x['id'])
                translation['id'] = maximum["id"] + 1
            else:
                translation['id'] = 0
            translation["_id"] = translation['type'] + "_" + str(translation["id"])
            created = self.repo_client.create(translation)
            if created.acknowledged:
                return {"message": None, "data": created.acknowledged}
            else:
                return {"message": "The translation can't be created", "data": created.acknowledged}

    def update_translation(self, translation):
        val = dt_val.validate_translation(translation)
        if val:
            return {"message": val, "data": False}

        el = self.find_all_translation({'type': translation['type'], 'name': translation['name']})
        if not el:
            return {"message": "Translation '{}' not present in DB".format(translation['name']), "data": False}

        if self.repo_client.update({'name': translation['name']}, translation) > 0:
            return {"message": None, "data": True}
        return {"message": "The translation can't be created", "data": False}

    def delete_translation(self, translation_type, name):
        records_affected = self.repo_client.delete({'type': translation_type, 'name': name})
        return records_affected > 0

    def delete_many(self, selector):
        self.repo_client.delete_all(selector)
