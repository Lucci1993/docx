from repository import Repository
from repository.mongo import MongoRepository


class Service(object):
    def __init__(self, repo_client=Repository(adapter=MongoRepository)):
        self.repo_client = repo_client

    def find_all_datamodel(self, selector):
        datamodel_list = self.repo_client.find_all(selector)
        return [datamodel for datamodel in datamodel_list]

    def create_datamodel(self, datamodel):
        created = self.repo_client.create(datamodel)
        if created.acknowledged:
            return {"message": None, "data": created.acknowledged}
        else:
            return {"message": "The disease can't be created", "data": created.acknowledged}
