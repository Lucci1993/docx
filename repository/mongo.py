from pymongo import MongoClient


class MongoRepository(object):
    def __init__(self):
        self.mongo_url = None
        self.db = None
        self.collection = None

    def initialize_db(self, db, collection, user, password):
        self.mongo_url = MongoClient("mongodb+srv://" + user + ":" + password + "@docx.lnfyc.mongodb.net/" + db
                                     + "?retryWrites=true&w=majority")
        self.db = self.mongo_url[db]
        self.collection = self.db[collection]

    def find_all(self, selector):
        return self.collection.find(selector)

    def find(self, selector):
        return self.collection.find_one(selector)

    def create(self, disease):
        return self.collection.insert_one(disease)

    def update(self, selector, disease):
        return self.collection.replace_one(selector, disease).modified_count

    def delete(self, selector):
        return self.collection.delete_one(selector).deleted_count

    def delete_all(self, selector):
        return self.collection.delete_many(selector)
