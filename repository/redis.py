import redis
import os
from repository.singleton import Singleton


@Singleton
class RedisRepository(object):
    def __init__(self):
        redis_host = os.environ.get('REDISHOST', 'localhost')
        redis_port = int(os.environ.get('REDISPORT', 6379))
        self._redis = redis.StrictRedis(host=redis_host, port=redis_port)

    def get_redis(self):
        return self._redis
