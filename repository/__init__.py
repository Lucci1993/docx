class Repository(object):
    def __init__(self, adapter=None):
        self.client = adapter()

    def initialize_db(self, db, collection, user, password):
        return self.client.initialize_db(db, collection, user, password)

    def find_all(self, selector):
        return self.client.find_all(selector)

    def find(self, selector):
        return self.client.find(selector)

    def create(self, disease):
        return self.client.create(disease)

    def update(self, selector, disease):
        return self.client.update(selector, disease)

    def delete(self, selector):
        return self.client.delete(selector)

    def delete_all(self, selector):
        return self.client.delete_all(selector)
