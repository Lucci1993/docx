from functools import wraps
from flask import request
from jwt import decode, exceptions
import utils.api_utils as api
import utils.cache_utils as cache
import utils.constants as ct


def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        authentication = request.headers.environ["HTTP_AUTHORIZATION"]
        if not authentication:
            return api.json_response("No authorization token provided!", status=403)

        try:
            token = authentication.split(' ')[1]
            resp = decode(token, ct.Constants.SECRET.value, verify=False, algorithms='HS256')

            if cache.get_redis_token() != token or not isinstance(resp, dict):
                raise exceptions.DecodeError
            else:
                cache.set_redis_auth(resp)
        except exceptions.DecodeError:
            return api.json_response("Invalid authorization token!", status=403)

        return f(*args, **kwargs)

    return wrap
