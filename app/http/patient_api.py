from flask import json, Blueprint, request
from repository.middlewares import login_required
import repository.service.patient_service as sv
import utils.api_utils as api
import utils.cache_utils as cache
from flask_cors import cross_origin


patient_api = Blueprint('patient_api', __name__)

service = sv.Service()


@cross_origin()
@patient_api.route("/<int:patient_id>", methods=['GET'])
@login_required
def get_by_id(patient_id):

    api.create_mongo_db_patient(cache.get_redis_auth(), service)
    patient = service.find_patient(patient_id)

    if patient:
        return api.json_response(patient)
    else:
        return api.json_response("Patient '{}' not found!".format(patient_id), status=404)


@cross_origin()
@patient_api.route("", methods=['POST'])
@login_required
def post():

    api.create_mongo_db_patient(cache.get_redis_auth(), service)
    patient = json.loads(request.data)

    result = service.create_patient(patient)

    if result["data"]:
        return api.json_response("Patient {} has been created!".format(patient['_id']), is_outcome=True)
    else:
        return api.json_response(result.pop("message"), status=400)


@cross_origin()
@patient_api.route("", methods=['PUT'])
@login_required
def update():

    api.create_mongo_db_patient(cache.get_redis_auth(), service)
    patient = json.loads(request.data)

    result = service.update_patient(patient)

    if result["data"]:
        return api.json_response("Patient {} has been update!".format(patient['_id']), is_outcome=True)
    else:
        return api.json_response(result.pop("message"), status=400)


@cross_origin()
@patient_api.route("/<int:patient_id>", methods=["DELETE"])
@login_required
def delete_by_id(patient_id):
    api.create_mongo_db_patient(cache.get_redis_auth(), service)

    if service.delete_patient(patient_id):
        return api.json_response("Patient '{}' has been deleted!".format(patient_id), is_outcome=True)
    else:
        return api.json_response("Patient '{}' not found!".format(patient_id), status=404)
