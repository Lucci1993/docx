from flask import json, Blueprint, request
import repository.service.translation_service as sv
import utils.api_utils as api
import utils.cache_utils as cache
from repository.middlewares import login_required
import utils.constants as ct
from flask_cors import cross_origin

translation_api = Blueprint('translation_api', __name__)

service = sv.Service()


@cross_origin()
@translation_api.route("", methods=['GET'])
@login_required
def get():
    translation_type = request.args.get('translationType')
    name = request.args.get('name')

    if not translation_type:
        return api.json_response("TranslationType query param must be fill!", status=403)
    elif check_translation_type(translation_type):
        return api.json_response(
            "TranslationType should belong to {}".format(ct.Constants.DATALIST_TYPES.value), status=403)

    api.create_mongo_db_translation(cache.get_redis_auth(), service)

    if name:
        selector = {'type': translation_type, 'name': name}
    else:
        selector = {'type': translation_type}

    return api.json_response(service.find_all_translation(selector))


@cross_origin()
@translation_api.route("", methods=['POST'])
@login_required
def post():
    translation_type = request.args.get('translationType')

    if not translation_type:
        return api.json_response("TranslationType query param must be fill!", status=403)
    elif check_translation_type(translation_type):
        return api.json_response(
            "TranslationType should belong to {}".format(ct.Constants.DATALIST_TYPES.value), status=403)

    api.create_mongo_db_translation(cache.get_redis_auth(), service)
    translation = json.loads(request.data)

    translation['type'] = translation_type
    result = service.create_translation(translation)

    if result["data"]:
        cache.set_redis_translation(translation_type, translation)
        return api.json_response("Translation {} has been created!".format(translation['id']), is_outcome=True)
    else:
        return api.json_response(result.pop("message"), status=400)


@cross_origin()
@translation_api.route("", methods=['PUT'])
@login_required
def update():
    translation_type = request.args.get('translationType')

    if not translation_type:
        return api.json_response("TranslationType query param must be fill!", status=403)
    elif check_translation_type(translation_type):
        return api.json_response(
            "TranslationType should belong to {}".format(ct.Constants.DATALIST_TYPES.value), status=403)

    api.create_mongo_db_translation(cache.get_redis_auth(), service)
    translation = json.loads(request.data)

    translation['type'] = translation_type
    result = service.update_translation(translation)

    if result["data"]:
        cache.set_redis_translation(translation_type, translation)
        return api.json_response("Translation {} has been update!".format(translation['name']), is_outcome=True)
    else:
        return api.json_response(result.pop("message"), status=400)


@cross_origin()
@translation_api.route("", methods=["DELETE"])
@login_required
def delete():
    translation_type = request.args.get('translationType')
    name = request.args.get('name')

    if not translation_type:
        return api.json_response("TranslationType query param must be fill!", status=403)
    elif check_translation_type(translation_type):
        return api.json_response(
            "TranslationType should belong to {}".format(ct.Constants.DATALIST_TYPES.value), status=403)
    elif not name:
        return api.json_response("Name query param must be fill!", status=403)

    api.create_mongo_db_translation(cache.get_redis_auth(), service)

    if service.delete_translation(translation_type, name):
        cache.delete_redis_translation(translation_type, name)
        return api.json_response("Translation '{}' has been deleted!".format(name), is_outcome=True)
    else:
        return api.json_response("Translation '{}' not found!".format(name), status=404)


def check_translation_type(translation_type):
    if translation_type not in ct.Constants.DATALIST_TYPES.value:
        return True
    return False
