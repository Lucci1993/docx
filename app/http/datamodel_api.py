from flask import Blueprint, json, request
import repository.service.datamodel_service as sv
import utils.api_utils as api
import utils.cache_utils as cache
import utils.data_validator as dt_val
import utils.datamodel_utils as dtm
from repository.middlewares import login_required
from flask_cors import cross_origin

datamodel_api = Blueprint('datamodel_api', __name__)

dt_service = sv.Service()


@cross_origin()
@datamodel_api.route("", methods=['GET'])
@login_required
def get():
    api.create_mongo_db_datamodel(cache.get_redis_auth(), dt_service)
    return api.json_response(dt_service.find_all_datamodel({}))


@cross_origin()
@datamodel_api.route("", methods=['POST'])
@login_required
def post():

    diseases = json.loads(request.data)

    api.create_mongo_db_datamodel(cache.get_redis_auth(), dt_service)
    datamodel, labels = dtm.generate_disease_datamodel(diseases)
    datamodel_db = dt_service.find_all_datamodel({})
    datamodel = dtm.generate_db_datamodel(datamodel_db, datamodel, labels)
    val = dt_val.validate_datamodel(datamodel)
    if val:
        return api.json_response(val, status=400)

    result = dt_service.create_datamodel(datamodel)
    if result["data"]:
        return api.json_response("Datamodel {} has been created in data {}!"
                                 .format(datamodel['_id'], datamodel["date"]), is_outcome=True)
    else:
        return api.json_response(result.pop("message"), status=400)
