from flask import request, json, Blueprint
import repository.service.disease_service as sv
import utils.api_utils as api
import utils.cache_utils as cache
from repository.middlewares import login_required
from flask_cors import cross_origin

disease_api = Blueprint('disease_api', __name__)

service = sv.Service()


@cross_origin()
@disease_api.route("/<int:disease_id>", methods=["GET"])
@login_required
def get_by_id(disease_id):
    api.create_mongo_db_disease(cache.get_redis_auth(), service)
    disease = service.find_disease(disease_id)

    if disease:
        return api.json_response(disease)
    else:
        return api.json_response("Disease '{}' not found!".format(disease_id), status=404)


@cross_origin()
@disease_api.route("", methods=['GET'])
@login_required
def get():
    api.create_mongo_db_disease(cache.get_redis_auth(), service)
    return api.json_response(service.find_all_diseases({}))


@cross_origin()
@disease_api.route("", methods=['POST'])
@login_required
def post():
    api.create_mongo_db_disease(cache.get_redis_auth(), service)
    disease = json.loads(request.data)
    result = service.create_disease(disease)

    if result["data"]:
        return api.json_response("Disease {} has been created!".format(disease['id']), is_outcome=True)
    else:
        return api.json_response(result.pop("message"), status=400)


@cross_origin()
@disease_api.route("", methods=['PUT'])
@login_required
def put():
    api.create_mongo_db_disease(cache.get_redis_auth(), service)
    disease = json.loads(request.data)

    result = service.update_disease(disease['id'], disease)

    if result["data"]:
        return api.json_response("Disease {} has been updated!".format(disease['id']), is_outcome=True)
    else:
        return api.json_response("Disease '{}' not found!".format(disease['id']), status=404)


@cross_origin()
@disease_api.route("/<int:disease_id>", methods=["PUT"])
@login_required
def update_by_id(disease_id):
    api.create_mongo_db_disease(cache.get_redis_auth(), service)
    disease = json.loads(request.data)

    result = service.update_disease(disease['id'], disease)

    if result["data"]:
        return api.json_response("Disease {} has been updated!".format(disease_id), is_outcome=True)
    else:
        return api.json_response("Disease '{}' not found!".format(disease_id), status=404)


@cross_origin()
@disease_api.route("/<int:disease_id>", methods=["DELETE"])
@login_required
def delete_by_id(disease_id):
    api.create_mongo_db_disease(cache.get_redis_auth(), service)

    if service.delete_disease(disease_id):
        return api.json_response("Disease {} has been deleted!".format(disease_id), is_outcome=True)
    else:
        return api.json_response("Disease '{}' not found!".format(disease_id), status=404)
