from flask import json, Blueprint, request
import repository.service.datalist_service as sv
import utils.api_utils as api
import utils.cache_utils as cache
from repository.middlewares import login_required
import utils.constants as ct
from flask_cors import cross_origin

datalist_api = Blueprint('datalist_api', __name__)

service = sv.Service()


@cross_origin()
@datalist_api.route("", methods=['GET'])
@login_required
def get():
    datalist_type = request.args.get('datalistType')
    name = request.args.get('name')
    id_key = request.args.get('id')

    if not datalist_type:
        return api.json_response("DatalistType query param must be fill!", status=403)
    elif check_datalist_type(datalist_type):
        return api.json_response(
            "DatalistType should belong to {}".format(ct.Constants.DATALIST_TYPES.value), status=403)

    api.create_mongo_db_datalist(cache.get_redis_auth(), service)

    if name:
        selector = {'type': datalist_type, 'name': name}
    elif id_key:
        selector = {'type': datalist_type, 'id': id_key}
    else:
        selector = {'type': datalist_type}

    return api.json_response(service.find_all_datalist(selector))


@cross_origin()
@datalist_api.route("", methods=['POST'])
@login_required
def post():
    datalist_type = request.args.get('datalistType')

    if not datalist_type:
        return api.json_response("DatalistType query param must be fill!", status=403)
    elif check_datalist_type(datalist_type):
        return api.json_response(
            "DatalistType should belong to {}".format(ct.Constants.DATALIST_TYPES.value), status=403)

    api.create_mongo_db_datalist(cache.get_redis_auth(), service)
    datalist = json.loads(request.data)

    datalist['type'] = datalist_type
    result = service.create_datalist(datalist)

    if result["data"]:
        cache.set_redis_datalist(datalist_type, datalist)
        return api.json_response("Datalist {} has been created!".format(datalist['id']), is_outcome=True)
    else:
        return api.json_response(result.pop("message"), status=400)


@cross_origin()
@datalist_api.route("", methods=['PUT'])
@login_required
def update():
    datalist_type = request.args.get('datalistType')

    if not datalist_type:
        return api.json_response("DatalistType query param must be fill!", status=403)
    elif check_datalist_type(datalist_type):
        return api.json_response(
            "DatalistType should belong to {}".format(ct.Constants.DATALIST_TYPES.value), status=403)

    api.create_mongo_db_datalist(cache.get_redis_auth(), service)
    datalist = json.loads(request.data)

    datalist['type'] = datalist_type
    result = service.update_datalist(datalist)

    if result["data"]:
        cache.set_redis_datalist(datalist_type, datalist)
        return api.json_response("Datalist {} has been update!".format(datalist['name']), is_outcome=True)
    else:
        return api.json_response(result.pop("message"), status=400)


@cross_origin()
@datalist_api.route("", methods=["DELETE"])
@login_required
def delete():
    datalist_type = request.args.get('datalistType')
    name = request.args.get('name')

    if not datalist_type:
        return api.json_response("DatalistType query param must be fill!", status=403)
    elif check_datalist_type(datalist_type):
        return api.json_response(
            "DatalistType should belong to {}".format(ct.Constants.DATALIST_TYPES.value), status=403)
    elif not name:
        return api.json_response("Name query param must be fill!", status=403)

    api.create_mongo_db_disease(cache.get_redis_auth(), service)

    if service.delete_datalist(datalist_type, name):
        cache.delete_redis_datalist(datalist_type, name)
        return api.json_response("Datalist '{}' has been deleted!".format(name), is_outcome=True)
    else:
        return api.json_response("Datalist '{}' not found!".format(name), status=404)


def check_datalist_type(datalist_type):
    if datalist_type not in ct.Constants.DATALIST_TYPES.value:
        return True
    return False

