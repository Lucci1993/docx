from flask import json, request, Blueprint, jsonify
from jwt import encode
import utils.api_utils as api
import utils.cache_utils as cache
import utils.constants as ct
from flask_cors import cross_origin

login_api = Blueprint('login_api', __name__)


@cross_origin()
@login_api.route("", methods=['POST'])
def post_token():

    credentials = json.loads(request.data)
    token = encode(credentials, ct.Constants.SECRET.value, algorithm='HS256')
    cache.set_redis_token(token)

    return api.json_response({'token': token})


@cross_origin()
@login_api.route("", methods=['DELETE'])
def delete_token():
    cache.delete_redis_auth_cache()
    return jsonify({})
