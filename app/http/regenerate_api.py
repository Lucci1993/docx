from flask import Blueprint
import repository.service.disease_service as di_sv
import repository.service.translation_service as tr_sv
import repository.service.datalist_service as dt_sv
from flask_cors import cross_origin
import utils.autoloader as auto
import utils.api_utils as api
from repository.middlewares import login_required

regenerate_api = Blueprint('regenerate_api', __name__)

dt_service = dt_sv.Service()
tr_service = tr_sv.Service()
di_service = di_sv.Service()

types = ['disease', 'riskFactor', 'specialist', 'symptom', 'instrumentalExamination', 'laboratoryTestsRange']


@cross_origin()
@regenerate_api.route("", methods=['POST'])
@login_required
def post_token():
    auto.regenerate(dt_service, tr_service, di_service, types)
    return api.json_response("Datalist, translations and diseases regenerated!", is_outcome=True)
