import threading
from flask import Blueprint, json, request
import utils.api_utils as api
from repository.middlewares import login_required
from flask_cors import cross_origin
import ml.trainer as tr
import ml.predictor as pr
import utils.patient_utils as patient

ml_api = Blueprint('ml_api', __name__)


@cross_origin()
@ml_api.route("/train/", methods=['POST'])
@login_required
def train():
    datamodel = json.loads(request.data)
    thread = threading.Thread(target=tr.train, kwargs={
        'datamodel': datamodel})
    thread.start()
    return api.json_response("Training has been started!", is_outcome=True)


@cross_origin()
@ml_api.route("/predict/", methods=['POST'])
@login_required
def predict():
    diagnosis = json.loads(request.data)

    val = patient.validate_diagnosis(diagnosis)
    if val:
        return api.json_response(val, status=400)

    predictor = pr.predict(diagnosis)

    return api.json_response(predictor)
