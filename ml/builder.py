import pandas as pd
from sklearn.model_selection import train_test_split
import utils.constants as ct
import logging as log


def preprocess_data(datamodel, maximum):
    # To have more data
    datamodel["model"] = datamodel["model"] * maximum
    ##########
    dataframe = pd.DataFrame(datamodel["model"], columns=datamodel["labels"])
    target = dataframe.pop(ct.Constants.DISEASE_DATAMODEL_NAME.value)
    return train_test_split(dataframe, target, test_size=ct.Constants.TEST_SET_PERCENTAGE.value, random_state=1)


def generate_prediction_output(y, y_predicted):
    prediction_list = list()
    for i in range(0, len(y_predicted.tolist()[0])):
        pred = y_predicted.tolist()[0][i]
        log.info(str(i) + ": " + str(pred * 100))
        prediction = dict()
        prediction["probability"] = pred * 100
        prediction["disease"] = i
        if i == int(y[0] - 1):
            log.info("The right is :" + str(y[0] - 1))
            prediction["predicted"] = True
        else:
            prediction["predicted"] = False
        prediction_list.append(prediction)
    return prediction_list
