from joblib import load
import utils.constants as ct
import os
import utils.datamodel_utils as data
import utils.patient_utils as patient
import numpy as np
import ml.builder as bl


def predict(diagnosis):
    model = _load_model()
    labels = data.generate_labels()
    features = patient.generate_features(diagnosis, labels)
    features = np.array(features).reshape(1, -1)
    y_predicted = model.predict_proba(features)
    y = model.predict(features)
    return bl.generate_prediction_output(y, y_predicted)


def _load_model():
    max_accuracy = (0, None)
    for f in os.listdir(ct.Constants.ML_FOLDER.value):
        name = os.path.basename(f).split("_")
        accuracy = float(name[1])
        if max_accuracy[0] < accuracy:
            max_accuracy = (accuracy, os.path.basename(f))
    return load(os.path.join(ct.Constants.ML_FOLDER.value, str(max_accuracy[1])))
