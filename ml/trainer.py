import ml.builder as bl
from sklearn import metrics
from sklearn.tree import DecisionTreeClassifier
from joblib import dump
import utils.constants as ct
from os import path
from datetime import date


def train(datamodel):
    x_train, x_test, y_train, y_test = bl.preprocess_data(datamodel, 100)

    model = DecisionTreeClassifier()

    model = model.fit(x_train, y_train)
    y_pred = model.predict(x_test)
    accuracy = metrics.accuracy_score(y_test, y_pred) * 100
    print("Accuracy Decision Tree: {:.2f}%".format(accuracy))

    _save_model(accuracy, model)


def _save_model(accuracy, model):
    if accuracy > ct.Constants.MIN_ACCURACY_ALLOWS.value:
        filename = "model_" + str(round(accuracy, 2)) + "_" + date.today().strftime("%d-%m-%Y")
        dump(model, path.join(ct.Constants.ML_FOLDER.value, filename))
