import utils.api_utils as api
import utils.cache_utils as cache
from structures.datalist import risk_factor, specialist, symptom, instrumental_examination, laboratory_tests_ranges
from structures.translation import disease_tr, risk_factor_tr, specialist_tr, symptom_tr, instrumental_examination_tr, \
    laboratory_tests_tr
import os
import json
import utils.constants as ct


def _generate_value_mapping_list():
    d = list()
    d.append([e[1] for e in risk_factor.RiskFactor.__members__.items()])
    d.append([e[1] for e in specialist.Specialist.__members__.items()])
    d.append([e[1] for e in symptom.Symptom.__members__.items()])
    d.append([e[1] for e in instrumental_examination.InstrumentalExamination.__members__.items()])
    d.append([e[1] for e in laboratory_tests_ranges.LaboratoryTestsRanges.__members__.items()])
    return d


def _generate_translator_mapping_list():
    d = list()
    d.append([e[1] for e in disease_tr.Disease.__members__.items()])
    d.append([e[1] for e in risk_factor_tr.RiskFactor.__members__.items()])
    d.append([e[1] for e in specialist_tr.Specialist.__members__.items()])
    d.append([e[1] for e in symptom_tr.Symptom.__members__.items()])
    d.append([e[1] for e in instrumental_examination_tr.InstrumentalExamination.__members__.items()])
    d.append([e[1] for e in laboratory_tests_tr.LaboratoryTests.__members__.items()])
    return d


# Delete all the datalist
def _regenerate_datalist_db(dt_service, types):
    api.create_mongo_db_datalist(cache.get_redis_auth(), dt_service)
    print("Removing datalist...")
    dt_service.delete_many({})
    print("Removed datalist...")
    data_list = _generate_value_mapping_list()
    print("Regenerating datalist...")
    for i in range(1, len(types)):
        for data in data_list[i-1]:
            datalist = dict()
            datalist['type'] = types[i]
            datalist['name'] = data.name
            datalist['value'] = data.value
            dt_service.create_datalist(datalist)
            print("Created: {} - {} - {}".format(types[i], data.name, data.value))
    print("Regenerated datalist...")


def _regenerate_translation_db(tr_service, types):
    api.create_mongo_db_translation(cache.get_redis_auth(), tr_service)
    print("Removing translations...")
    tr_service.delete_many({})
    print("Removed translations...")
    trans_list = _generate_translator_mapping_list()
    print("Regenerating translations...")
    for i in range(0, len(types)):
        for data in trans_list[i]:
            translation = dict()
            translation['type'] = types[i]
            translation['name'] = data.name
            translation['translations'] = data.value
            tr_service.create_translation(translation)
            print("Created: {} - {} - {}".format(types[i], data.name, data.value))
    print("Regenerated translations...")


def _regenerate_diseases_db(di_service):
    api.create_mongo_db_disease(cache.get_redis_auth(), di_service)
    print("Removing diseases...")
    di_service.delete_many({})
    print("Removed diseases...")
    print("Regenerating diseases...")
    for file in os.listdir(ct.Constants.JSON_FOLDER.value):
        with open(ct.Constants.JSON_FOLDER.value + file) as json_file:
            dis = json.load(json_file)
            di_service.create_disease(dis)
            print("Created: disease - {}".format(dis['id']))
    print("Regenerated diseases...")


def regenerate(dt_service, tr_service, di_service, types):
    print("---------- START ------------")
    _regenerate_datalist_db(dt_service, types)
    _regenerate_translation_db(tr_service, types)
    _regenerate_diseases_db(di_service)
    print("---------- DONE ------------")
