import enum


class Constants(enum.Enum):
    AVRO_DISEASE_FILE = "structures/avro/Disease.avsc"
    AVRO_TRANSLATION_FILE = "structures/avro/Translation.avsc"
    JSON_FOLDER = "structures/json/"
    TEST_FOLDER = "structures/test/"
    MATRIX_HEIGHT = 3
    TRANSLATION_LANGUAGES = ["ENG", "ITA"]
    NORMALIZATION_RANGE = (-1, 1)
    NORMALIZATION_STANDARD = (-10000000, 10000000)
    NORMALIZATION_STABILIZER = 10 ** 3
    NORMALIZATION_MAX = 100
    DB_NAME = "diseases"
    COLLECTION_DISEASE_NAME = "diseases"
    COLLECTION_DATAMODEL_NAME = "datamodels"
    COLLECTION_DATALIST_NAME = "datalists"
    COLLECTION_TRANSLATION_NAME = "translation"
    DISEASE_PREFIX = "/disease"
    DATAMODEL_PREFIX = "/datamodel"
    LOGIN_PREFIX = "/auth"
    DATALIST_PREFIX = "/datalist"
    TRANSLATION_PREFIX = "/translation"
    ML_PREFIX = "/ml"
    REGENERATE_PREFIX = "/regenerate"
    AVRO_DATAMODEL_FILE = "structures/avro/Datamodel.avsc"
    SECRET = '6372rv8bdbd28v2GSIA'
    TOKEN = 'token'
    AUTH = 'auth'
    DATALIST_TYPES = ['disease', 'instrumentalExamination', 'laboratoryTests', 'laboratoryTestsRanges', 'riskFactor',
                      'specialist', 'symptom']
    DISEASE_DATAMODEL_NAME = "DiseaseName"
    AVRO_DIAGNOSIS_FILE = "structures/avro/Diagnosis.avsc"

    # ML values
    TEST_SET_PERCENTAGE = 0.3
    MIN_ACCURACY_ALLOWS = 70
    ML_FOLDER = "ml/model/"

    # Patient values
    PATIENT_PREFIX = "/patient"
    COLLECTION_PATIENT_NAME = "patient"
    AVRO_PATIENT_FILE = "structures/avro/Patient.avsc"
