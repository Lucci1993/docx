import fastavro as fa
import re
from utils import constants as ct
from structures.datalist import disease, risk_factor, specialist, symptom, instrumental_examination, laboratory_tests, \
    laboratory_tests_ranges
from structures.translation import disease_tr, risk_factor_tr, specialist_tr, symptom_tr, instrumental_examination_tr, \
    laboratory_tests_tr
import logging as log


def validate_json(data):
    schema = fa.schema.load_schema(ct.Constants.AVRO_DISEASE_FILE.value)
    error_msg = None
    try:
        fa.validate(data, schema)
        val = _validate_mapping(data, _generate_value_mapping_list(), None)
        if not val:
            val = _validate_laboratory_tests_ranges()
            if not val:
                val = _validate_translation()
                if val:
                    error_msg = "Validation failed for translation: {}.".format(val)
                else:
                    log.info("Validation succeed!")
            else:
                error_msg = "Validation failed for laboratory test range: {}.".format(val)
        else:
            error_msg = "Validation failed with mapping class: {}\nCannot find id: {}.".format(val[0], val[1])
    except BaseException as e:
        error_msg = "Avro schema validation failed for json with error:\n{}.".format(e)
    finally:
        if error_msg:
            log.error(error_msg)
        return error_msg


def _validate_mapping(data, mapping_list, value):
    for k, v in data.items():
        if k == "id":
            if not value and v not in mapping_list[0][1]:
                return "disease", v
            elif value and v not in [item[1] for item in mapping_list if item[0] in value][0]:
                return value, v
        if isinstance(v, list) and _check_matching(k, [item[0] for item in mapping_list]):
            value = k.lower()
            for el in v:
                if isinstance(el, dict):
                    if _validate_mapping(el, mapping_list, value):
                        return value, el["id"]
                elif el not in [item[1] for item in mapping_list if item[0] in value][0]:
                    return value, el
        elif isinstance(v, dict) and _check_matching(k, [item[0] for item in mapping_list]):
            if _validate_mapping(v, mapping_list, k.lower()):
                return k.lower(), v["id"]
    return None


def _generate_value_mapping_list():
    d = list()
    d.append((disease.Disease.__name__.lower(), [e[1].value for e in disease.Disease.__members__.items()]))
    d.append((risk_factor.RiskFactor.__name__.lower(),
              [e[1].value for e in risk_factor.RiskFactor.__members__.items()]))
    d.append((specialist.Specialist.__name__.lower(), [e[1].value for e in specialist.Specialist.__members__.items()]))
    d.append((symptom.Symptom.__name__.lower(), [e[1].value for e in symptom.Symptom.__members__.items()]))
    d.append((instrumental_examination.InstrumentalExamination.__name__.lower(),
              [e[1].value for e in instrumental_examination.InstrumentalExamination.__members__.items()]))
    d.append((laboratory_tests.LaboratoryTests.__name__.lower(),
              [e[1].value for e in laboratory_tests.LaboratoryTests.__members__.items()]))
    return d


def _check_matching(attribute, mapping_list):
    for elem in mapping_list:
        if elem in attribute.lower():
            return True
    return False


def _validate_laboratory_tests_ranges():
    laboratory_tests_list = [e[0] for e in laboratory_tests.LaboratoryTests.__members__.items()]
    laboratory_tests_range_list = [e[1] for e in laboratory_tests_ranges.LaboratoryTestsRanges.__members__.items()]
    laboratory_tests_range_list_name = [e[0] for e in laboratory_tests_ranges.LaboratoryTestsRanges.__members__.items()]

    for element in laboratory_tests_list:
        if element not in laboratory_tests_range_list_name:
            return element

    for element in laboratory_tests_range_list:

        # Length should be equal 3 the third element of the array should be a string and for each range there should
        # be a specular in the laboratory_test datalist
        if isinstance(element.value, list) and (len(element.value) != 3 or not isinstance(element.value[2], str)
                                                or element.name not in laboratory_tests_list):
            return element
        elif isinstance(element.value, dict):
            for k, v in element.value.items():
                if re.match("([0-9]{1,3}-[M|F])|([M|F])", k) \
                        and (len(v) != 3 or not isinstance(v[2], str) or element.name not in laboratory_tests_list):
                    return element
    return None


def _generate_mapping_list():
    d = list()
    d.append((disease.Disease.__name__.lower(), [e[1] for e in disease.Disease.__members__.items()]))
    d.append((risk_factor.RiskFactor.__name__.lower(), [e[1] for e in risk_factor.RiskFactor.__members__.items()]))
    d.append((specialist.Specialist.__name__.lower(), [e[1] for e in specialist.Specialist.__members__.items()]))
    d.append((symptom.Symptom.__name__.lower(), [e[1] for e in symptom.Symptom.__members__.items()]))
    d.append((instrumental_examination.InstrumentalExamination.__name__.lower(),
              [e[1] for e in instrumental_examination.InstrumentalExamination.__members__.items()]))
    d.append((laboratory_tests.LaboratoryTests.__name__.lower(),
              [e[1] for e in laboratory_tests.LaboratoryTests.__members__.items()]))
    return d


def _generate_translator_mapping_list():
    d = list()
    d.append((disease_tr.Disease.__name__.lower(), [e[1] for e in disease_tr.Disease.__members__.items()]))
    d.append((risk_factor_tr.RiskFactor.__name__.lower(),
              [e[1] for e in risk_factor_tr.RiskFactor.__members__.items()]))
    d.append((specialist_tr.Specialist.__name__.lower(),
              [e[1] for e in specialist_tr.Specialist.__members__.items()]))
    d.append((symptom_tr.Symptom.__name__.lower(),
              [e[1] for e in symptom_tr.Symptom.__members__.items()]))
    d.append((instrumental_examination_tr.InstrumentalExamination.__name__.lower(),
              [e[1] for e in instrumental_examination_tr.InstrumentalExamination.__members__.items()]))
    d.append((laboratory_tests_tr.LaboratoryTests.__name__.lower(),
              [e[1] for e in laboratory_tests_tr.LaboratoryTests.__members__.items()]))
    return d


def _validate_translation():
    name_mapping_list = _generate_mapping_list()
    translator_mapping_list = _generate_translator_mapping_list()
    for element_tr in translator_mapping_list:
        for el_tr in element_tr[1]:
            # Check length of the translation array
            if len(el_tr.value) != len(ct.Constants.TRANSLATION_LANGUAGES.value):
                return element_tr[0] + "." + el_tr.name

    for element in name_mapping_list:
        for element_tr in translator_mapping_list:
            if element[0] == element_tr[0]:
                for el in element[1]:
                    # Check if all the enums have been translated
                    if el.name not in [e.name for e in element_tr[1]]:
                        return element[0] + "." + el.name
    return None


def validate_datamodel(datamodel):
    schema = fa.schema.load_schema(ct.Constants.AVRO_DATAMODEL_FILE.value)
    try:
        fa.validate(datamodel, schema)
    except BaseException as e:
        error_msg = "Avro schema validation failed for json datamodel with error:\n{}.".format(e)
        log.error(error_msg)
        return error_msg
    return None


def validate_translation(translation):
    schema = fa.schema.load_schema(ct.Constants.AVRO_TRANSLATION_FILE.value)
    try:
        fa.validate(translation, schema)
        for tr in translation['translations']:
            if not isinstance(tr, str):
                error_msg = "{} is not a string.".format(tr)
                log.error(error_msg)
                return error_msg
    except BaseException as e:
        error_msg = "Avro schema validation failed for json datamodel with error:\n{}.".format(e)
        log.error(error_msg)
        return error_msg
    return None


def validate_patient(patient):
    schema = fa.schema.load_schema(ct.Constants.AVRO_PATIENT_FILE.value)
    error_msg = None
    try:
        fa.validate(patient, schema)
    except BaseException as e:
        error_msg = "Avro schema validation failed for json datamodel with error:\n{}.".format(e)
        log.error(error_msg)
    finally:
        return error_msg
