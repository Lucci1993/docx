from repository.redis import RedisRepository
import utils.constants as ct
import json


def get_redis_token():
    r = RedisRepository.instance()
    return r.get_redis().get(ct.Constants.TOKEN.value).decode("utf-8")


def set_redis_token(token):
    r = RedisRepository.instance()
    r.get_redis().set(ct.Constants.TOKEN.value, token)


def get_redis_auth():
    r = RedisRepository.instance()
    return r.get_redis().get(ct.Constants.AUTH.value).decode("utf-8")


def set_redis_auth(auth):
    r = RedisRepository.instance()
    r.get_redis().set(ct.Constants.AUTH.value, json.dumps(auth))


def delete_redis_auth_cache():
    r = RedisRepository.instance()
    r.get_redis().delete(ct.Constants.TOKEN.value)


def get_redis_datalist(value):
    r = RedisRepository.instance()
    root_dict = json.loads(r.get_redis().get(ct.Constants.COLLECTION_DATALIST_NAME.value).decode("utf-8"))
    return root_dict[value]


def set_redis_datalist(value, obj):
    r = RedisRepository.instance()
    if not r.get_redis().get(ct.Constants.COLLECTION_DATALIST_NAME.value):
        r.get_redis().set(ct.Constants.COLLECTION_DATALIST_NAME.value, json.dumps(dict()))
    entity_list = list()
    root_dict = r.get_redis().get(ct.Constants.COLLECTION_DATALIST_NAME.value).decode("utf-8")
    root_dict = json.loads(root_dict)
    if value in root_dict:
        entity_list = root_dict[value]
        if obj not in entity_list:
            if obj["name"] in [a["name"] for a in entity_list]:
                entity_list = [a for a in entity_list if a["name"] != obj["name"]]
            entity_list.append(obj)
    else:
        entity_list.append(obj)
    root_dict[value] = entity_list
    r.get_redis().set(ct.Constants.COLLECTION_DATALIST_NAME.value, json.dumps(root_dict))


def delete_redis_datalist(datalist_type, name):
    r = RedisRepository.instance()
    root_dict = r.get_redis().get(ct.Constants.COLLECTION_DATALIST_NAME.value).decode("utf-8")
    root_dict = json.loads(root_dict)
    entity_list = root_dict[datalist_type]
    entity_list = [a for a in entity_list if a["name"] != name]
    root_dict[datalist_type] = entity_list
    r.get_redis().set(ct.Constants.COLLECTION_DATALIST_NAME.value, json.dumps(root_dict))


def get_redis_translation(value):
    r = RedisRepository.instance()
    root_dict = json.loads(r.get_redis().get(ct.Constants.COLLECTION_TRANSLATION_NAME.value).decode("utf-8"))
    return root_dict[value]


def set_redis_translation(value, obj):
    r = RedisRepository.instance()
    if not r.get_redis().get(ct.Constants.COLLECTION_TRANSLATION_NAME.value):
        r.get_redis().set(ct.Constants.COLLECTION_TRANSLATION_NAME.value, json.dumps(dict()))
    entity_list = list()
    root_dict = r.get_redis().get(ct.Constants.COLLECTION_TRANSLATION_NAME.value).decode("utf-8")
    root_dict = json.loads(root_dict)
    if value in root_dict:
        entity_list = root_dict[value]
        if obj not in entity_list:
            if obj["name"] in [a["name"] for a in entity_list]:
                entity_list = [a for a in entity_list if a["name"] != obj["name"]]
            entity_list.append(obj)
    else:
        entity_list.append(obj)
    root_dict[value] = entity_list
    r.get_redis().set(ct.Constants.COLLECTION_TRANSLATION_NAME.value, json.dumps(root_dict))


def delete_redis_translation(datalist_type, name):
    r = RedisRepository.instance()
    root_dict = r.get_redis().get(ct.Constants.COLLECTION_TRANSLATION_NAME.value).decode("utf-8")
    root_dict = json.loads(root_dict)
    entity_list = root_dict[datalist_type]
    entity_list = [a for a in entity_list if a["name"] != name]
    root_dict[datalist_type] = entity_list
    r.get_redis().set(ct.Constants.COLLECTION_TRANSLATION_NAME.value, json.dumps(root_dict))
