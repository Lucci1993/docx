import fastavro as fa
import logging as log
import utils.constants as ct
import utils.datamodel_utils as dt
from structures.datalist import laboratory_tests_ranges


def validate_diagnosis(diagnosis):
    schema = fa.schema.load_schema(ct.Constants.AVRO_DIAGNOSIS_FILE.value)
    error_msg = None
    try:
        fa.validate(diagnosis, schema)
    except BaseException as e:
        error_msg = "Avro schema validation failed for json with error:\n{}.".format(e)
    finally:
        log.error(error_msg)
        return error_msg


def generate_features(diagnosis, labels):
    features = [0] * len(labels)
    for ids in diagnosis["symptoms"]:
        features[labels.index(ids)] = 1
    for ids in diagnosis["riskFactors"]:
        features[labels.index(ids)] = 1
    for data in diagnosis["laboratoryTests"]:
        normalized_value = _normalize_value(data["key"], data["value"])
        features[labels.index(data["key"])] = normalized_value
    return features


def _normalize_value(name, value):
    section = list()
    if "_" in name:
        section = name.split("_")
        name = section[0]

    norm_range = ct.Constants.NORMALIZATION_RANGE.value
    test_range = [e[1].value for e in laboratory_tests_ranges.LaboratoryTestsRanges.__members__.items()
                  if e[0] == name][0]

    if isinstance(test_range, list):
        return dt.normalize_value(test_range[0], test_range[1], value, norm_range[0], norm_range[1])
    else:
        test_range = test_range[section[1]]
        return dt.normalize_value(test_range[0], test_range[1], value, norm_range[0], norm_range[1])
