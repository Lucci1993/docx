from flask import jsonify
from utils.constants import Constants
import json


def create_mongo_db(service, db, collection, user, password):
    service.repo_client.initialize_db(db=db, collection=collection, user=user, password=password)


def json_response(payload, is_outcome=False, status=200):
    if status != 200:
        payload = {'success': False, 'message': payload}
    elif is_outcome:
        payload = {'success': True, 'message': payload}
    return jsonify(payload), status


def create_mongo_db_datalist(auth, service):
    auth = json.loads(auth)
    if not service.repo_client.client.mongo_url:
        service.repo_client.initialize_db(db=Constants.DB_NAME.value,
                                          collection=Constants.COLLECTION_DATALIST_NAME.value,
                                          user=auth['user'], password=auth['password'])


def create_mongo_db_datamodel(auth, service):
    auth = json.loads(auth)
    if not service.repo_client.client.mongo_url:
        service.repo_client.initialize_db(db=Constants.DB_NAME.value,
                                          collection=Constants.COLLECTION_DATAMODEL_NAME.value,
                                          user=auth['user'], password=auth['password'])


def create_mongo_db_disease(auth, service):
    auth = json.loads(auth)
    if not service.repo_client.client.mongo_url:
        service.repo_client.initialize_db(db=Constants.DB_NAME.value,
                                          collection=Constants.COLLECTION_DISEASE_NAME.value,
                                          user=auth['user'], password=auth['password'])


def create_mongo_db_translation(auth, service):
    auth = json.loads(auth)
    if not service.repo_client.client.mongo_url:
        service.repo_client.initialize_db(db=Constants.DB_NAME.value,
                                          collection=Constants.COLLECTION_TRANSLATION_NAME.value,
                                          user=auth['user'], password=auth['password'])


def create_mongo_db_patient(auth, service):
    auth = json.loads(auth)
    if not service.repo_client.client.mongo_url:
        service.repo_client.initialize_db(db=Constants.DB_NAME.value,
                                          collection=Constants.COLLECTION_PATIENT_NAME.value,
                                          user=auth['user'], password=auth['password'])
