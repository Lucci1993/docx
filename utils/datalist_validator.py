import utils.constants as ct
import utils.cache_utils as cache
import re


def validate_laboratory_test_range(dictonary, name):
    if not dictonary or not dictonary['Standard']:
        return "Laboratory test range should has at least a 'standard' value"

    laboratory_tests_list = cache.get_redis_datalist(ct.Constants.DATALIST_TYPES.value[2])
    laboratory_tests_list = [test['name'] for test in laboratory_tests_list]

    for k, v in dictonary.items():

        if k == 'Standard':
            # Length should be equal 3 the third element of the array should be a string and for each range there should
            # be a specular in the laboratory_test datalist
            standard = dictonary['Standard']
            if isinstance(standard, list) and (len(standard) != 3 or not isinstance(standard[2], str)
                                               or name not in laboratory_tests_list):
                return "Error in standard list {} : {}".format(k, v)
            if name not in laboratory_tests_list:
                return "A laboratory test with name '{}' " \
                       "should be added before defining its relative range with this name".format(name)

        else:
            if not re.search("([0-9]{1,3}-[M|F])|([M|F])", k) or len(v) != 3 or not isinstance(v[2], str):
                return "Error in list {} : {}".format(k, v)
            if name not in laboratory_tests_list:
                return "A laboratory test with name '{}' " \
                       "should be added before defining its relative range with this name".format(name)

    return None


def validate_datalist(datalist):
    if isinstance(datalist['name'], str) and datalist['name'].isalpha():
        if datalist['type'] == ct.Constants.DATALIST_TYPES.value[3]:
            return validate_laboratory_test_range(datalist['value'], datalist['name'])
        else:
            return None
    else:
        return "Name should be composed only of characters!"
