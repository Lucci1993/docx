from datetime import datetime
import math
from utils import constants as ct
from structures.datalist import risk_factor, symptom, laboratory_tests, laboratory_tests_ranges
from functools import reduce
import operator


def generate_disease_datamodel(diseases):
    array_list = list()
    labels = [ct.Constants.DISEASE_DATAMODEL_NAME.value] + generate_labels()
    for disease in diseases:
        array = _generate_array(disease)
        array_list.append(array)

    return [_generate_datamodel(obj, labels) for obj in array_list], labels


def _generate_array(data):
    array = list(list())
    array.append([(data["id"], ct.Constants.DISEASE_DATAMODEL_NAME.value)])

    symptoms_list = list()
    for element in [(obj["correlation"], obj["id"]) for obj in data["symptoms"]]:
        for sym in [e[1] for e in symptom.Symptom.__members__.items()]:
            if sym.value == element[1]:
                symptoms_list.append((element[0], sym.name))
    array.append(symptoms_list)

    risk_factors_list = list()
    for element in [(obj["correlation"], obj["id"]) for obj in data["riskFactors"]]:
        for risk in [e[1] for e in risk_factor.RiskFactor.__members__.items()]:
            if risk.value == element[1]:
                risk_factors_list.append((element[0], risk.name))
    array.append(risk_factors_list)

    array.append([_normalize_laboratory_test_values(obj["correlation"], obj["range"], obj["id"])
                  for obj in data["laboratoryTests"]])
    array[3] = reduce(operator.add, array[3])

    return reduce(operator.add, array)


def _generate_datamodel(array, labels):
    data = [0] * len(labels)
    for value, name in array:
        data[labels.index(name)] = value
    return data


# It normalize the laboratory test value
# If you have in input the age of the patient you have to normalize two time the value: the first time for the
# standard interval of the attribute, and the second time for the NORMALIZATION_RANGE
def _normalize_laboratory_test_values(correlation, range_list, identifier):
    name = [e[0] for e in laboratory_tests.LaboratoryTests.__members__.items() if e[1].value == identifier][0]
    test_range = \
        [e[1].value for e in laboratory_tests_ranges.LaboratoryTestsRanges.__members__.items() if e[0] == name][0]
    norm_range = ct.Constants.NORMALIZATION_RANGE.value
    normalized_list = list()
    for elem in range_list:
        if isinstance(test_range, list):
            normalized_value = \
                normalize_value(test_range[0], test_range[1], elem['value'], norm_range[0], norm_range[1])
        else:
            test_range = test_range[elem['key']]
            normalized_value = \
                normalize_value(test_range[0], test_range[1], elem['value'], norm_range[0], norm_range[1])

        if len(range_list) > 1:
            normalized_list.append((calculate_function(normalized_value, correlation), name + "_" + elem['key']))
        else:
            normalized_list.append((calculate_function(normalized_value, correlation), name))

    return normalized_list


def normalize_value(minimum, maximum, x, a, b):
    return ((b - a) * ((x - minimum) / (maximum - minimum))) + a


def calculate_function(x, gamma):
    if math.fabs(x) > ct.Constants.NORMALIZATION_MAX.value:
        x = ct.Constants.NORMALIZATION_MAX.value
    if ct.Constants.NORMALIZATION_RANGE.value[0] <= x <= ct.Constants.NORMALIZATION_RANGE.value[1]:
        return 0
    else:
        value = (math.e ** (gamma * math.fabs(x))) - (gamma * math.fabs(x) * (math.e ** gamma))
        return value / ct.Constants.NORMALIZATION_STABILIZER.value


def generate_db_datamodel(datamodel_db, datamodel_list, labels):
    maximum_id = -1
    if datamodel_db:
        maximum_id = max([x['_id'] for x in datamodel_db])
    datamodel = dict()
    datamodel["_id"] = maximum_id + 1
    datamodel["date"] = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    datamodel["labels"] = labels
    datamodel["model"] = datamodel_list
    return datamodel


def generate_labels():
    lb = list()
    for e in symptom.Symptom.__members__.items():
        lb.append(e[0])
    for e in risk_factor.RiskFactor.__members__.items():
        lb.append(e[0])
    for element in laboratory_tests_ranges.LaboratoryTestsRanges.__members__.items():
        el = element[1].value
        if isinstance(el, list):
            lb.append(element[0])
        else:
            for k in el.keys():
                lb.append(element[0] + "_" + k)
    return lb
