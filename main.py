from flask import Flask
from flask_cors import CORS
from app.http.disease_api import disease_api
from app.http.datamodel_api import datamodel_api
from app.http.login_api import login_api
from app.http.datalist_api import datalist_api
from app.http.translation_api import translation_api
from app.http.ml_api import ml_api
from app.http.patient_api import patient_api
from app.http.regenerate_api import regenerate_api
import utils.constants as ct


app = Flask(__name__)
cors = CORS(app)
app.register_blueprint(disease_api, url_prefix=ct.Constants.DISEASE_PREFIX.value)
app.register_blueprint(datamodel_api, url_prefix=ct.Constants.DATAMODEL_PREFIX.value)
app.register_blueprint(login_api, url_prefix=ct.Constants.LOGIN_PREFIX.value)
app.register_blueprint(datalist_api, url_prefix=ct.Constants.DATALIST_PREFIX.value)
app.register_blueprint(translation_api, url_prefix=ct.Constants.TRANSLATION_PREFIX.value)
app.register_blueprint(ml_api, url_prefix=ct.Constants.ML_PREFIX.value)
app.register_blueprint(patient_api, url_prefix=ct.Constants.PATIENT_PREFIX.value)
app.register_blueprint(regenerate_api, url_prefix=ct.Constants.REGENERATE_PREFIX.value)


if __name__ == "__main__":
    app.run(debug=True, use_debugger=False, use_reloader=False, passthrough_errors=True)
